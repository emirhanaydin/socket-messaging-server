const mongoose = require('mongoose')
const crypto = require('crypto')
const jwt = require('jsonwebtoken')
const uniqueValidator = require('mongoose-unique-validator')
require('mongoose-long')(mongoose)
const Long = mongoose.Schema.Types.Long

const { Schema } = mongoose

let SECRET

const UsersSchema = new Schema({
  email: { type: String, index: true, required: true, unique: true },
  name: { type: String, required: true },
  rooms: [{ type: Schema.Types.ObjectId, ref: 'Rooms' }],
  createdAt: { type: Long, default: Date.now },
  updatedAt: Long,
  hash: { type: String, required: true },
  salt: { type: String, required: true }
})

UsersSchema.plugin(uniqueValidator)

UsersSchema.methods.setPassword = function (password) {
  this.salt = crypto.randomBytes(16).toString('hex')
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex')
}

UsersSchema.methods.validatePassword = function (password) {
  const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex')
  return this.hash === hash
}

UsersSchema.methods.generateJWT = function () {
  const today = new Date()
  const expirationDate = new Date(today)
  expirationDate.setDate(today.getDate() + 60)

  return jwt.sign({
    _id: this._id,
    email: this.email,
    name: this.name,
    rooms: this.rooms,
    createdAt: this.createdAt,
    updatedAt: this.updatedAt,
    exp: Math.trunc(expirationDate.getTime() / 1000)
  }, SECRET)
}

UsersSchema.methods.toAuthJSON = function () {
  return {
    _id: this._id,
    name: this.name,
    email: this.email,
    rooms: this.rooms,
    createdAt: this.createdAt,
    updatedAt: this.updatedAt,
    token: this.generateJWT()
  }
}

mongoose.model('Users', UsersSchema)

module.exports = function (secret) {
  SECRET = secret
}
