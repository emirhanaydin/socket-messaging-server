const mongoose = require('mongoose')
require('mongoose-long')(mongoose)
const Long = mongoose.Schema.Types.Long

const { Schema } = mongoose

const MessagesSchema = new Schema({
  body: { type: String, required: true },
  sender: { type: Schema.Types.ObjectId, ref: 'Users', required: true },
  room: { type: Schema.Types.ObjectId, ref: 'Rooms', required: true },
  createdAt: { type: Long, default: Date.now },
  updatedAt: Long
})

mongoose.model('Messages', MessagesSchema)
