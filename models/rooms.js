const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')
require('mongoose-long')(mongoose)
const Long = mongoose.Schema.Types.Long

const { Schema } = mongoose

const RoomsSchema = new Schema({
  name: { type: String, required: true, unique: true },
  roomType: { type: String, required: true },
  admin: { type: Schema.Types.ObjectId, ref: 'Users' },
  users: [{ type: Schema.Types.ObjectId, ref: 'Users' }],
  messages: [{ type: Schema.Types.ObjectId, ref: 'Messages' }],
  iconName: String,
  createdAt: { type: Long, default: Date.now },
  updatedAt: Long
})

RoomsSchema.plugin(uniqueValidator)

mongoose.model('Rooms', RoomsSchema)
