const mongoose = require('mongoose')
const router = require('express').Router()
const Rooms = mongoose.model('Rooms')

router.post('/', (req, res) => {
  const roomObj = req.body.room
  roomObj.createdAt = Date.now()

  const room = new Rooms(roomObj)

  return room.save()
    .then(value => res.json({ room: value }))
    .catch(res.send)
})

module.exports = router
