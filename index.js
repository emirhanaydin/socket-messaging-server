const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const session = require('express-session')
const cors = require('cors')
const mongoose = require('mongoose')
const errorHandler = require('errorhandler')
const socketioJwt = require('socketio-jwt')
const status = require('http-status')

const PORT = process.env.PORT || 23058
const SECRET = process.env.SECRET || 'secret'
const isProduction = process.env.NODE_ENV === 'production'

const app = express()

app.use(cors())
app.use(require('morgan')('dev'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static(path.join(__dirname, 'public')))
app.use(session({ secret: 'socket-messaging', cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false }))

if (!isProduction) {
  app.use(errorHandler())
}

// Configure Mongoose
mongoose.connect('mongodb://localhost/socket-messaging', { useNewUrlParser: true }).catch(console.error)
mongoose.set('debug', !isProduction)

require('./models/users')(SECRET)
require('./models/rooms')
require('./models/messages')
require('./config/passport')
app.use(require('./routes'))

const http = require('http').createServer(app)
const io = require('socket.io')(http)

http.listen(PORT, function () {
  console.log(`listening on *:${PORT}`)
})

const today = new Date()
const expirationDate = new Date(today)
expirationDate.setDate(today.getDate() + 60)

const Users = mongoose.model('Users')
const Rooms = mongoose.model('Rooms')
const Messages = mongoose.model('Messages')

io.sockets.on('connection', socketioJwt.authorize({
  secret: SECRET,
  timeout: Math.trunc(expirationDate.getTime() / 1000)
})).on('authenticated', function (socket) {
  console.log(`authenticated user: ${socket.decoded_token.name}`)

  socket
    .on('req-users', () => {
      Users.find()
        .then(value => socket.emit('res-users', value))
        .catch(reason => socket.emit('error', reason))
    })
    .on('message', args => sendMessage(socket, args))
    .on('messages', args => getMessages(socket, args))
    .on('rooms', args => getRooms(socket, args))
    .on('join', args => joinRoom(socket, args))
    .on('create_room', args => createRoom(socket, args))
})

async function sendMessage (socket, args) {
  try {
    console.log(args)

    const message = args.message

    const messages = new Messages(message)
    message.createdAt = (await messages.save())._doc.createdAt

    const room = await Rooms.findById(message.room)

    await Rooms.updateOne({ name: room.name }, {
      $push: { messages: messages._id },
      updatedAt: Date.now()
    })

    message.sender = (await Users.findById(message.sender))._doc
    socket.broadcast.emit('message', { message: message })
  } catch (e) {
    console.error(e)
    socket.emit('error', e)
  }
}

async function getRooms (socket, args) {
  try {
    const user = args.user

    const { _doc: { rooms } } = (await Users.findById(user._id, 'rooms').populate('rooms'))
    for (let i = 0; i < rooms.length; i++) {
      rooms[i] = rooms[i]._doc
    }

    console.log(rooms)
    socket.emit('rooms', rooms)
  } catch (e) {
    console.error(e)
    socket.emit('error', e)
  }
}

async function createRoom (socket, args) {
  const room = args.room
  const user = args.user

  let resultRoom
  if (room.name) {
    resultRoom = (await Rooms.findOne({ name: room.name }).catch())._doc
  }

  if (!resultRoom) {
    resultRoom = room

    if (room.roomType === 'public') {
      resultRoom.users = [user._id]

      const rooms = new Rooms(resultRoom)
      await rooms.save()
    } else if (room.roomType === 'private') {
      const otherEmail = room.users[0].email
      const otherUser = (await Users.findOne({ email: otherEmail }).catch())._doc

      if (!otherUser) {
        throw new Error(`No users found with ${otherEmail}`)
      }

      // Rooms.findOne({
      //   roomType: 'private',
      //   users: {
      //     $all: [user._id, otherUser._id]
      //   }
      // })

      resultRoom.name = user.email
      const rooms = new Rooms(resultRoom)
      await rooms.save()

      Users.findOneAndUpdate({ _id: user._id }, {
        $push: { rooms: resultRoom }
      })

      Users.update({
        _id: {
          $in: [user._id, otherUser._id]
        }
      }, { $push: { rooms: resultRoom._id } })

      resultRoom.name = otherEmail
      resultRoom.users = [user._id, otherUser._id]
    } else {
      throw new Error('Illegal roomType')
    }
  } else {
    switch (room.roomType) {
      case 'public':
        if (!resultRoom.users.contains(user._id)) {
          resultRoom.users.push(user._id)
        }
        break
      case 'private':
        throw new Error('Illegal private roomType format')
      default:
        throw new Error('Illegal roomType')
    }
  }

  socket.join(resultRoom.name)
  socket.emit('create_room', resultRoom)
}

async function joinRoom (socket, args) {
  try {
    console.log(`join: ${args}`)

    const room = args.room
    const user = args.user

    let resultRoom = (await Rooms.findOne({ 'name': room.name }).catch())._doc
    if (!resultRoom) {
      resultRoom = room
      resultRoom.admin = user._id
      resultRoom.users = [user._id]
      resultRoom.messages = []

      const rooms = new Rooms(resultRoom)
      resultRoom._id = rooms.id
      await rooms.save()
    }

    const condition = { _id: resultRoom._id, 'users': { $nin: [user._id] } }
    await Rooms.findOneAndUpdate(condition, {
      $push: { users: user._id }
    })

    socket.join(room.name)
    room._id = resultRoom._id
    const { _doc: { messages } } = (await Rooms.findById(resultRoom._id).select('messages').populate({
      path: 'messages',
      model: 'Messages'
    }))

    for (let i = 0; i < messages.length; i++) {
      messages[i] = messages[i]._doc
      messages[i].sender = (await Users.findById(messages[i].sender))._doc
    }

    room.messages = messages
    socket.emit('join', { 'room': room })
  } catch (e) {
    console.error(e)
    socket.emit('error', e)
  }
}

async function getMessages (socket, args) {
  try {
    const room = args.room

    const messages = await Rooms.findOne({ name: room.name }, 'messages')
    socket.emit('messages', messages)
  } catch (e) {
    console.error(e)
    socket.emit('error', e)
  }
}
